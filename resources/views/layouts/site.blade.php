<!doctype html>
<html lang="pt-br">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('titulo') Blog do Martin</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
  </head>
  <body>

        <div class="container">
            <header class="d-flex flex-wrap justify-content-center py-3 mb-4 border-bottom">
              <a href="/" class="d-flex align-items-center mb-3 mb-md-0 me-md-auto text-dark text-decoration-none">
                <svg class="bi me-2" width="40" height="32"><use xlink:href="{{route('home')}}"></use></svg>
                <span class="fs-4">Dev Martin</span>
              </a>

              <ul class="nav nav-pills">
                <li class="nav-item"><a href="{{route('home')}}" class="btn btn-outline-secondary" aria-current="page">Home</a></li>
                <li class="nav-item"><a href="{{route('sobre')}}" class="nav-link" style="color: black;">Sobre</a></li>
              </ul>
            </header>
          </div>

        <main>
            @yield('conteudo')
          <section class="py-5 text-center container">
            <div class="row py-lg-5">
              <div class="col-lg-6 col-md-8 mx-auto">
                <h1 class="fw-light">Meus Projetos</h1>
                <p class="lead text-muted">Ola meu nome e Martin seja bem vindo(a) ao me portfólio, Venha conhecer meus projetos e me acompanhar nessa longa carreira do Back-end.</p>
              </div>
            </div>
          </section>

          <div class="album py-5 bg-light">
            <div class="container">

              <div class="row row-cols-1 row-cols-sm-2 row-cols-md-3 g-3">
                <div class="col">
                  <div class="card shadow-sm">
                    <img src="/img/prego.jfif" alt="">
                    <div class="card-body">
                      <p class="card-text">O Prego Digital surgiu por meio da necessidade de organizar e rasgar de vez as famosas anotações em papeis de embalagens de pão ou em cadernetas, onde muitas vezes se perde ou estraga por diversos motivos deixando o proprietário do estabelecimento com o prejuízo da dívida não paga pelo seu devedor.</p>
                      <div class="d-flex justify-content-between align-items-center">
                        <div class="btn-group">
                            <a href="https://gitlab.com/martin-senac-backend/prego-digital3">
                          <button type="button" class="btn btn-sm btn-outline-secondary">Ver projeto no gitlab</button>
                        </a>
                        </div>
                        <small class="text-muted">Prego Digital</small>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col">
                  <div class="card shadow-sm">
                    <img src="/img/blog news.jfif" alt="">
                    <div class="card-body">
                      <p class="card-text">O blog news surgiu por uma simples ideia de eu querer montar algum site de noticias sobre ti, criar postagens sobre softwares, hardwares tudo relacionado sobre o mundo da tecnologia.</p>
                      <div class="d-flex justify-content-between align-items-center">
                        <div class="btn-group">
                            <a href="https://gitlab.com/martin-senac-backend/blog-news">
                          <button type="button" class="btn btn-sm btn-outline-secondary">Ver projeto no gitlab</button>
                        </a>
                        </div>
                        <small class="text-muted">Blog News</small>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col">
                  <div class="card shadow-sm">
                    <img src="/img/todolist.PNG" alt="">
                    <div class="card-body">
                      <p class="card-text">O TodoList e um listador de tarefas que tem o intuito de organizar sua rotina de trabalho e adicionar pendencias não feitas.</p>
                      <div class="d-flex justify-content-between align-items-center">
                        <div class="btn-group">
                            <a href="https://gitlab.com/martin-senac-backend/sistema-todolist">
                            <button type="button" class="btn btn-sm btn-outline-secondary">Ver projeto no gitlab</button>
                        </a>
                        </div>
                        <small class="text-muted">TodoList</small>


        </main>

        <div class="container">
            <footer class="py-3 my-4">
              <p class="text-center text-muted">&copy; 2022 Empresa | Dev Martin</p>
            </footer>
          </div>

            <script src="/docs/5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN" crossorigin="anonymous"></script>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js" integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN" crossorigin="anonymous"></script>
  </body>
</html>
