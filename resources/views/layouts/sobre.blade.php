<!doctype html>
<html lang="pt-br">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('titulo') Blog do Martin</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
  </head>
  <body>
    <div class="container">
        <header class="d-flex flex-wrap justify-content-center py-3 mb-4 border-bottom">
          <a href="/" class="d-flex align-items-center mb-3 mb-md-0 me-md-auto text-dark text-decoration-none">
            <svg class="bi me-2" width="40" height="32"><use xlink:href="{{route('home')}}"></use></svg>
            <span class="fs-4">Dev Martin</span>
          </a>

          <ul class="nav nav-pills">
            <li class="nav-item"><a href="{{route('home')}}" class="btn btn-outline-secondary" aria-current="page">Home</a></li>
            <li class="nav-item"><a href="{{route('sobre')}}" class="nav-link" style="color: black;">Sobre</a></li>
          </ul>
        </header>
      </div>

      @yield('conteudo')
    <div class="container col-xxl-8 px-4 py-5">
        <div class="row flex-lg-row-reverse align-items-center g-5 py-5">
          <div class="col-10 col-sm-8 col-lg-6">
            <img src="/img/martin.jpg" class="d-block mx-lg-auto img-fluid" alt="Bootstrap Themes" width="250" height="250" loading="lazy">
          </div>
          <div class="col-lg-6">
            <h1 class="display-5 fw-bold lh-1 mb-3">Um pouco mais sobre min !</h1>
            <p class="lead">Gosto de desenvolver sites com a linguagem PHP utilizo o framework Láravel, tenho conhecimentos em HTML e CSS básicos utilizando o framework Bootstrap 5, Tenho conhecimentos em banco de dados relacionais e não relacionais (Mysql) e tenho experiencias com as ferramentas Git.
                Gosto de aprender coisas novas sou apaixonado por tecnologia eu amo programar tenho muita disponibilidade gosto de participar de feedbacks sobre projetos dar opiniões e também gosto de interagir!
                Competências técnicas: PHP, Larável, Mysql, HTML E CSS básicos utilizando o framework Bootstrap 5 e Git.</p>
            <div class="d-grid gap-2 d-md-flex justify-content-md-start">
            </div>
          </div>
        </div>
      </div>

      <div class="container">
        <footer class="py-3 my-4">
          <p class="text-center text-muted">&copy; 2022 Empresa | Dev Martin</p>
        </footer>
      </div>


    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js" integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN" crossorigin="anonymous"></script>
  </body>
</html>
